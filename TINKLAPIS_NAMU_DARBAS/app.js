// Meniu (burger'io) dinamika susitraukiant langui

var burgerdiv = document.querySelector('.hide');
var saugiklis = true;

function show() {
    if (saugiklis) {
        burgerdiv.classList.add('on');
        // console.log(burgerdiv);
        saugiklis = false;
    } else {
        burgerdiv.classList.remove('on')
            // console.log(burgerdiv);
        saugiklis = true;
    }
}

// Formos uzpildymo logika

function validate() {
    var error = document.getElementById('error_message');
    error.setAttribute("style", "color: red; font-size: 1.5em");

    var name = document.getElementById('name').value;
    var subject = document.getElementById('subject').value;
    var phone = document.getElementById('phone').value;
    var email = document.getElementById('email').value;
    var message = document.getElementById('message').value;

    var errorText;

    if (name.length < 3) {
        errorText = "Minimalus vardo raidziu skaicius yra 3";
        error.innerHTML = errorText;
        return false;
    }
    if (subject.length < 10) {
        errorText = "Minimalus temos simboliu skacius yra 10";
        error.innerHTML = errorText;
        return false;
    }
    if (isNaN(phone) || phone.length != 9) {
        errorText = "Ivedete netinkama numeri";
        error.innerHTML = errorText;
        return false;
    }
    if (email.indexOf("@") == -1 || email.length < 5) {
        errorText = "Ivedete netinkama elektronini pasta";
        error.innerHTML = errorText;
        return false;
    }
    if (message.length < 40) {
        errorText = "Ivestas zinutes tekstas yra per trumpas";
        error.innerHTML = errorText;
        return false;
    }
    alert('Forma uzpildyta sekmingai!')
    return true;
}