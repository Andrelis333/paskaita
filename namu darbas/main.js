$(document).ready(function () {
    // Musu kintamieji
    var lentele = $('#pagrindiniai_duomenys tbody');
    var duomenys = [
        {
            numeris: 'ASD234',
            greitis: 321,
            laikas: 123,
            data: '2010-12-10'
        },
        {
            numeris: 'GDS234',
            greitis: 643,
            laikas: 543,
            data: '2010-12-10'
        },
        {
            numeris: 'ASD234',
            greitis: 765,
            laikas: 087,
            data: '2010-12-10'
        },
        {
            numeris: 'ASD234',
            greitis: 456,
            laikas: 876,
            data: '2010-12-10'
        },
    ];
    var index;

    // lenteles atnaujinimo funkcija, kuria galime pernaudoti tiek kartu, kiek spausdinsim is naujo lentele 
    function atnaujintiLentele() {
        lentele.empty();
        for (i = 0; i < duomenys.length; i++) {
            var tr = $('<tr></tr>');
            var numerisTd = $('<td>' + duomenys[i].numeris + '</td>');
            var greitisTd = $('<td>' + duomenys[i].greitis + '</td>');
            var laikasTd = $('<td>' + duomenys[i].laikas + '</td>');
            var dataTd = $('<td>' + duomenys[i].data + '</td>');
            var taisytiTd = $('<td><button type="button" class="btn btn-success taisyti" data-toggle="modal" data-target="#edit_modal">Taisyti</button></td>');
            var trintiTd = $('<td><button type="button" class="btn btn-danger trinti" data-toggle="modal" data-target="#trinti_modalas">Trinti</button></td>');
            tr.append(numerisTd).append(greitisTd).append(laikasTd).append(dataTd).append(taisytiTd).append(trintiTd);
            lentele.append(tr);
        }
    }
    // kad duomenys butu lenteleje iskarto ijungus puslapi
    atnaujintiLentele();

    // Objekto konstruktorius
    function Automobilis(numeris, greitis, laikas, data) {
        this.numeris = numeris;
        this.greitis = greitis;
        this.laikas = laikas;
        this.data = data;
    }

    // idedam duomenis    
    $('#ivesti').click(function () {
        // Gausim duomenis is inputu
        var numeris = $('#numeris').val();
        var greitis = $('#greitis').val();
        var laikas = $('#laikas').val();
        var data = $('#data').val();
        var masina = new Automobilis(numeris, greitis, laikas, data);
        duomenys.push(masina);
        atnaujintiLentele();
    });
   
    // Trinti duomenis
    $('#pagrindiniai_duomenys').on('click', '.trinti', function () {
        index = $(this).parent().parent().index();
    });

    $('#istrinti').click(function () {
        duomenys.splice(index, 1);
        atnaujintiLentele();
    });

    // taisyti Duomenis
    $('#pagrindiniai_duomenys').on('click', '.taisyti', function () {
        index = $(this).parent().parent().index();
        // gaunam senas reiksmes is masyvo
        var numeris = duomenys[index].numeris;
        var greitis = duomenys[index].greitis;
        var laikas = duomenys[index].laikas;
        var data = duomenys[index].data;
        // sudedam senas reiksmes i inputus
        $('#numeris_edit').val(numeris);
        $('#greitis_edit').val(greitis);
        $('#laikas_edit').val(laikas);
        $('#data_edit').val(data);
        console.log(duomenys);
    });

    $('#redaguoti').click(function () {
        // gaunam naujas inputu reiksmes
        var numeris = $('#numeris_edit').val();
        var greitis = $('#greitis_edit').val();
        var laikas = $('#laikas_edit').val();
        var data = $('#data_edit').val();
        var naujasObjektas = new Automobilis(numeris, greitis, laikas, data);
        console.log(naujasObjektas);
        duomenys.splice(index, 1, naujasObjektas);
        console.log(index);
        console.log(duomenys);
        atnaujintiLentele();
    });
});